---
layout: default
title: About
snippet: Just your common or garden about page.
---
<img src="/assets/TramFishers.gif" alt="Title GIF" style="width: 300px;"/>

Welcome to glenelg.io, a site set aside for writing, sharing, grumbling and (maybe) ranting.

I'm Fraser Smith, a fifty mumble mumble something year old developer and amateur photographer living in Shanghai. I'm also responsible for the @Literaverse and @BardInsults twitter bots.
